rc_dlist_deque - Doubly-linked list based on std::Rc
====================================================

Please see the
[documentation](https://www.chiark.greenend.org.uk/~ianmdlvl/rc-dlist-deque/rc_dlist_deque/)
(online copy).

Source code, issues, etc., are here:
https://salsa.debian.org/iwj/rc-dlist-deque

Copyright (C) 2019-2022 Ian Jackson; GNU GPLv3+; NO WARRANTY.
