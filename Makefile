
# rc-dlist-deque - Doubly-linked list based on `std::Rc` -- Makefile
#
#  Copyright (C) 2019-2022 Ian Jackson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This Makefile has some convenient targets, but you can build just
# with cargo if you like.

CARGO=cargo
SHELL=bash

o=>$@.tmp && mv -f $@.tmp $@

all: doc cargo--test

full: cargo-release--test

cargo--%:
	+$(CARGO) $*

cargo-release--%:
	+$(CARGO) $* --release

www=ianmdlvl@chiark:public-html/rc-dlist-deque

lib-doc.md: src/lib.rs Makefile
	sed -n 's#dlist/index\.html#rc_dlist_deque/&#; s/^\/\/! *//p' <$< $o

target/doc/index.html: lib-doc.md
	mkdir -p target/doc
	set -e; set -o pipefail; \
	title="$$(sed -n 's/^\# //; s/`//g; 1p' $<)"; \
	sed 1d $< | pandoc --standalone -Mtitle="$$title" $o

doc:	cargo--doc target/doc/index.html

doc-to-www:
	git clean -xdff
	$(MAKE) doc
	rsync -aH --delete target/doc/. $(www)/.

# I have a ~/.cargo/config which avoids willy-nilly use of crates.io,
# because I don't like my computer to automatically download and
# execute code from th internet.
#
# Unfortunately there is no sane way to override this for a particular
# run of cargo, right now.  Related issues:
#  https://github.com/rust-lang/cargo/issues/6699
#  https://github.com/rust-lang/cargo/issues/6728
#
# So I use this insane way to override this.  AFAICT there is no way
# that an ordinary user on a shared machine can use this workaround
# unless the sysadmin is willing to grant them a second file area
# outside their home directory.  Yuk.

otherdir=/volatile/ian-alt/cargo-argh/rc-dlist-deque

publish:
	mkdir -p $(otherdir)
	rm -rf $(otherdir)
	mkdir $(otherdir)
	git clone . $(otherdir)
	cd $(otherdir) && HOME=`cd .. && pwd` $(CARGO) publish
